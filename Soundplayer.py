#!/usr/bin/python

import os
import threading

from Constants import Constants

if os.uname()[1] == Constants.RASPBERRYPI_HOSTNAME: ### RASPBERRY PI ###

    import pygame

    # Play the sound
    def playSound(name):
        sound = pygame.mixer.Sound(name)
        pygame.mixer.Sound.play(sound)

    # Soundplayer class for multithreading
    class Soundplayer(threading.Thread):
        def __init__(self, file_name, loop):
            threading.Thread.__init__(self)
            self.file_name = file_name
            self.loop = loop

        def run(self):
            pygame.mixer.init(44100)
            pygame.mixer.music.load(self.file_name)
            pygame.mixer.music.set_volume(1.0)

            if(self.loop):
                pygame.mixer.music.play(-1)
            else: 
                pygame.mixer.music.play()

            #while pygame.mixer.music.get_busy() == True:
            #    pass

        def stop(self): 
            pygame.mixer.music.stop()

else: ### MACOSX ###

    import signal
    import sys
    import subprocess

    # Play the sound
    def playSound(name):
        os.system("afplay {}&".format(name))

    # Soundplayer class for multithreading
    class Soundplayer(threading.Thread):
        def handler(self, signum, frame):
            self.stop()
            sys.exit(0)

        def __init__(self, file_name, loop):
            threading.Thread.__init__(self)
            self.file_name = file_name
            self.loop = loop

            signal.signal(signal.SIGINT, self.handler)

        def run(self):
            while self.loop: 
                self.subprocess = subprocess.Popen(["afplay", self.file_name])
                self.subprocess.wait()

        def stop(self): 
            self.loop = False
            if hasattr(self, 'subprocess'): self.subprocess.terminate()