import RPi.GPIO as GPIO
import time

from Constants import Constants

def buttonPressed(channel):
    switcher = {
        Constants.GPIO_PIN_PLAYER_A_UP: 'Player A: Up',
        Constants.GPIO_PIN_PLAYER_A_DOWN: 'Player A: Down',
        Constants.GPIO_PIN_PLAYER_A_SOUND: 'Player A: Sound',

        Constants.GPIO_PIN_PLAYER_B_UP: 'Player B: Up',
        Constants.GPIO_PIN_PLAYER_B_DOWN: 'Player B: Down',
        Constants.GPIO_PIN_PLAYER_B_SOUND: 'Player B: Sound',

        Constants.GPIO_PIN_MENU: 'Menu',
        Constants.GPIO_PIN_SUBMIT: 'Submit',
    }

    print("{}".format(switcher.get(channel, "UNKNOWN")))
    

# player a
GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_PLAYER_A_UP, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_A_UP, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_PLAYER_A_DOWN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_A_DOWN, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_PLAYER_A_SOUND, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_A_SOUND, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

# player b
GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_PLAYER_B_UP, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_B_UP, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_PLAYER_B_DOWN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_B_DOWN, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_PLAYER_B_SOUND, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_B_SOUND, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

# submit and menu
GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_MENU, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_MENU, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

GPIO.setmode(GPIO.BCM)
GPIO.setup(Constants.GPIO_PIN_SUBMIT, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.add_event_detect(Constants.GPIO_PIN_SUBMIT, GPIO.RISING, callback = buttonPressed, bouncetime = Constants.GPIO_BOUNCETIME)

def main():
    while True:
        time.sleep(0.1)

try: main()
except KeyboardInterrupt: pass
finally: GPIO.cleanup()

