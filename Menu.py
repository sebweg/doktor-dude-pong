# Menu Class
import turtle as t

from Constants import Constants

class Button:
    def __init__(self, text, size, color, y):
        self.text = text
        self.size = size
        self.pen = t.Turtle()
        self.pen.hideturtle()
        self.pen.penup()
        self.pen.color(color)
        self.pen.goto(0,y)

    def clear(self):
        self.pen.clear()

    def draw(self):
        self.pen.clear()
        self.pen.write(self.text,align="center",font=("Monaco",self.size,"normal"))

class Menu:
    def __init__(self):
        self.selected_button_index = 0          # store which button is selected

        # Title and buttons
        self.title = Button(Constants.TITLE, 30, Constants.COLOR_TITLE, 150)
        self.new_game_button = Button(Constants.MENU_BUTTON_NEW_GAME, 24, Constants.COLOR_BUTTON, 25)
        self.quit_game_button = Button(Constants.MENU_BUTTON_QUIT_GAME, 24, Constants.COLOR_BUTTON, -25)

        # Active button border
        self.active_border_button = t.Turtle()
        self.active_border_button.hideturtle()
        self.active_border_button.penup()
        self.active_border_button.color(Constants.COLOR_BUTTON)

    def activeButtonChanged(self):
        if self.selected_button_index == 0:
            self.selected_button_index = 1
        else: 
            self.selected_button_index = 0

    def drawRectangle(self, x, y, width, height):
        self.active_border_button.clear()
        self.active_border_button.up()
        self.active_border_button.goto(x, y)
        self.active_border_button.down()
        self.active_border_button.forward(width)          
        self.active_border_button.left(90)
        self.active_border_button.forward(height)
        self.active_border_button.left(90)
        self.active_border_button.forward(width)
        self.active_border_button.left(90)
        self.active_border_button.forward(height)
        self.active_border_button.left(90)

    def clear(self):
        self.title.clear()
        self.new_game_button.clear()
        self.quit_game_button.clear()
        self.active_border_button.clear()

    def update(self):
        self.title.draw()
        self.new_game_button.draw()
        self.quit_game_button.draw()

        if self.selected_button_index == 0:
            self.drawRectangle(-110, 20, 220, 40)
        else: 
            self.drawRectangle(-110, -30, 220, 40)