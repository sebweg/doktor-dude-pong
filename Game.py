import turtle as t

from Constants import Constants
from Soundplayer import playSound
from game_objects.Player import Player
from game_objects.Score import Score
from game_objects.Ball import Ball
from game_objects.Countdown import Countdown

# Game class
class Game:
    def __init__(self):
        # Creating the two players
        self.player_a = Player(Constants.IMG_DOKTORDUDE, Constants.PLAYER_NAME_DOKTORDUDE, -350)
        self.player_b = Player(Constants.IMG_DUDE, Constants.PLAYER_NAME_DUDE, 350)

        self.score = Score(self.player_a, self.player_b)    # Creating the score

        self.ball = Ball(self)                              # Creating a pong ball for the game

        self.countdown = Countdown()                        # Creating a countdown-thread

    def movePlayer(self, is_up, is_player_a):
        if self.countdown.is_running: return                # No player movement while countdown active

        if is_up:
            if is_player_a: self.player_a.moveUp()
            else: self.player_b.moveUp()
        else:
            if is_player_a: self.player_a.moveDown()
            else: self.player_b.moveDown()

    def clear(self):
        self.score.clear()

        self.player_a.clear()
        self.player_b.clear()

        self.ball.clear()

        self.countdown.clear()

    def update(self):
        if self.countdown.is_running:                       # No movement while countdown active
            self.countdown.draw()
        else:
            
            self.ball.move()                                # Moving the ball

            result = self.ball.checkBorderCollision()       # Cheking for border collision
            if not result == None:
                if result == Constants.PLAYER_NAME_DOKTORDUDE: self.player_a.incrementScore()
                elif result == Constants.PLAYER_NAME_DUDE: self.player_b.incrementScore()

                self.countdown.start()
                playSound(Constants.SOUND_WALLHIT)

            if self.ball.checkPaddleCollision():            # Checking for paddle collision
                playSound(Constants.SOUND_PADDLEHIT) 
            
            self.score.draw()                               # Drawing Score
            self.player_a.draw()                            # Drawing Player A 
            self.player_b.draw()                            # Drawing Player B

