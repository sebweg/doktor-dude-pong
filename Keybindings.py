import os
import threading

from Constants import Constants

if os.uname()[1] == Constants.RASPBERRYPI_HOSTNAME: # Raspberry Pi imports
    import RPi.GPIO as GPIO
    import signal
    import time

# Setup keybindings for macosx
def setupMacOSXKeybindings(win, upPressedB, downPressedB, upPressedA, downPressedA, onSubmit, openMenu, onPlayShuffleSound):
    
    win.listen()

    win.onkeypress(upPressedB, "Up")
    win.onkeypress(downPressedB, "Down")
    win.onkeypress(onPlayShuffleSound, "p")

    win.onkeypress(upPressedA, "e")
    win.onkeypress(downPressedA, "d")
    win.onkeypress(onPlayShuffleSound, "r")

    win.onkeypress(onSubmit, "space")
    win.onkeypress(openMenu, "q")

# Connectorthread to rpio of raspberry
class RPIOConnector(threading.Thread):
    def handler(self, signum, frame):
        self.stop()

    def __init__(self, upPressedB, downPressedB, upPressedA, downPressedA, onSubmit, openMenu, onPlayShuffleSound):
        threading.Thread.__init__(self)
        self.is_running = False

        self.upPressedB = upPressedB
        self.downPressedB = downPressedB

        self.upPressedA = upPressedA
        self.downPressedA = downPressedA

        self.onPlayShuffleSound = onPlayShuffleSound

        self.onSubmit = onSubmit
        self.openMenu = openMenu

        signal.signal(signal.SIGINT, self.handler)

    def run(self):
        if self.is_running: return

        GPIO.setmode(GPIO.BCM)

        # player a
        GPIO.setup(Constants.GPIO_PIN_PLAYER_A_UP, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_A_UP, GPIO.RISING, callback = self.upPressedA, bouncetime = Constants.GPIO_BOUNCETIME)

        GPIO.setup(Constants.GPIO_PIN_PLAYER_A_DOWN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_A_DOWN, GPIO.RISING, callback = self.downPressedA, bouncetime = Constants.GPIO_BOUNCETIME)

        GPIO.setup(Constants.GPIO_PIN_PLAYER_A_SOUND, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_A_SOUND, GPIO.RISING, callback = self.onPlayShuffleSound, bouncetime = Constants.GPIO_BOUNCETIME)

        # player b
        GPIO.setup(Constants.GPIO_PIN_PLAYER_B_UP, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_B_UP, GPIO.RISING, callback = self.upPressedB, bouncetime = Constants.GPIO_BOUNCETIME)

        GPIO.setup(Constants.GPIO_PIN_PLAYER_B_DOWN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_B_DOWN, GPIO.RISING, callback = self.downPressedB, bouncetime = Constants.GPIO_BOUNCETIME)

        GPIO.setup(Constants.GPIO_PIN_PLAYER_B_SOUND, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_PLAYER_B_SOUND, GPIO.RISING, callback = self.onPlayShuffleSound, bouncetime = Constants.GPIO_BOUNCETIME)

        # submit and menu
        GPIO.setup(Constants.GPIO_PIN_MENU, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_MENU, GPIO.RISING, callback = self.openMenu, bouncetime = Constants.GPIO_BOUNCETIME)

        GPIO.setup(Constants.GPIO_PIN_SUBMIT, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
        GPIO.add_event_detect(Constants.GPIO_PIN_SUBMIT, GPIO.RISING, callback = self.onSubmit, bouncetime = Constants.GPIO_BOUNCETIME)


        self.is_running = True
        while self.is_running: time.sleep(0.1)
           
        GPIO.cleanup()

    def stop(self): 
        self.is_running = False
