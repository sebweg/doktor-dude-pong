#!/usr/bin/env python3

import turtle as t
import os
import random
import sys

from Constants import Constants
from Menu import Menu
from Game import Game
from Keybindings import setupMacOSXKeybindings, RPIOConnector
from Soundplayer import Soundplayer, playSound
from Videoplayer import playVideo

# Show intro movie
if len(sys.argv) <= 1:
    playVideo(Constants.MOVIE_INTRO, Constants.MOVIE_INTRO_AUDIO)
elif not sys.argv[1] == "--novideo":
    playVideo(Constants.MOVIE_INTRO, Constants.MOVIE_INTRO_AUDIO)

# Variables
quit_game = False                                       # quit game in loop
is_game_running = False                                 # game or menu
is_raspi = False                                        # running on raspberry or macos

# Checking if game is running on raspberry or macos
if os.uname()[1] == Constants.RASPBERRYPI_HOSTNAME: is_raspi = True

# Screen / window
win = t.Screen()                                        # creating a window
win.title(Constants.TITLE)                              # giving name to the game
win.register_shape(Constants.IMG_BOWLING_BALL)          # register images
win.register_shape(Constants.IMG_DOKTORDUDE)
win.register_shape(Constants.IMG_DUDE)
win.bgpic(Constants.IMG_BOWLING_BG)                     # setting the backgroundpic
if is_raspi:
    win.setup(width=1.0,height=1.0,startx=0,starty=0)   # size of the game panel for raspberry
    #win.getcanvas()._root().overrideredirect(True)     # disables the applicationbar
else:
    win.setup(width=800,height=480,startx=0,starty=0)   # size of the game panel for workstation
win.tracer(0)                                           # which speed up's the game

# Views
menu = Menu()                                           # menu view
game = Game()                                           # game view

# Functions
def openMenu(channel=None):
    global is_game_running
    is_game_running = False

def onSubmit(channel=None):
    global is_game_running, quit_game

    if not is_game_running:
        if menu.selected_button_index == 0:
            is_game_running = True
        else:
            quit_game = True

def onPlayShuffleSound(channel=None):
    random_array_index = random.randint(0, len(Constants.SOUND_SHUFFLE_ARRAY) - 1)
    playSound(Constants.SOUND_SHUFFLE_ARRAY[random_array_index])

def upPressedA(channel=None):
    if is_game_running:
        game.movePlayer(True, True)
    else:
        menu.activeButtonChanged()

def downPressedA(channel=None):
    if is_game_running:
        game.movePlayer(False, True)
    else:
        menu.activeButtonChanged()

def upPressedB(channel=None):
    if is_game_running:
        game.movePlayer(True, False)
    else:
        menu.activeButtonChanged()

def downPressedB(channel=None):
    if is_game_running:        
        game.movePlayer(False, False)
    else:
        menu.activeButtonChanged()

# Background track
soundplayer = Soundplayer(Constants.SOUND_BACKGROUND, True)
soundplayer.start()

# Keybindings
if is_raspi:
    rpio_connector = RPIOConnector(upPressedB, downPressedB, upPressedA, downPressedA, onSubmit, openMenu, onPlayShuffleSound)
    rpio_connector.start()
else:
    setupMacOSXKeybindings(win, upPressedB, downPressedB, upPressedA, downPressedA, onSubmit, openMenu, onPlayShuffleSound)

# Main loop
while True:
    win.update()

    if quit_game:           # Quit/exit the game
        if is_raspi:
            rpio_connector.stop()
            rpio_connector.join()

        soundplayer.stop()
        soundplayer.join()
        win.bye()
        break
    
    if is_game_running:     # while game is running
        menu.clear()
        game.update()
    else:                   # show menu
        game.clear()
        menu.update()

    