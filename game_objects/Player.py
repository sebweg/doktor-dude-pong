import turtle as t
import os

from Constants import Constants

# Player class
class Player:
    def __init__(self, img, name, x):
        self.score = 0
        self.name = name
        self.x = x

        self.paddle = t.Turtle(img)
        self.paddle.speed(0)
        self.paddle.shapesize(stretch_wid=5,stretch_len=1)
        self.paddle.penup()
        self.paddle.goto(x,0)

        self.y = self.paddle.ycor()

        if os.uname()[1] == Constants.RASPBERRYPI_HOSTNAME:
            self.ycor_movement_points = 30
        else:
            self.ycor_movement_points = 15

    def clear(self):
        self.paddle.goto(self.x,0)

    def incrementScore(self):
        self.score = self.score + 1

    def moveUp(self):
        self.y = self.paddle.ycor()
        self.y = self.y + self.ycor_movement_points

    def moveDown(self):
        self.y = self.paddle.ycor()
        self.y = self.y - self.ycor_movement_points

    def draw(self):
        self.paddle.sety(self.y)
