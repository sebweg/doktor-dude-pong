import turtle as t
import os

from Constants import Constants

# Ball class
class Ball:
    def __init__(self, game):
        self.game = game

        self.ball = t.Turtle(Constants.IMG_BOWLING_BALL)
        self.ball.speed(0)
        self.ball.penup()
        self.ball.goto(0,0)

        if os.uname()[1] == Constants.RASPBERRYPI_HOSTNAME:
            self.ball_dxy_init = 1.5
            self.ball_dxy_speed_increment = 1.0
        else:
            self.ball_dxy_init = 2.5
            self.ball_dxy_speed_increment = 2.0
            
        self.ball_dx = self.ball_dxy_init
        self.ball_dy = self.ball_dxy_init

    def move(self):
        self.ball.setx(self.ball.xcor() + self.ball_dx)
        self.ball.sety(self.ball.ycor() + self.ball_dy)

    def resetSpeed(self):
        if(self.ball_dx >= 0):
            self.ball_dx = self.ball_dxy_init
            self.ball_dy = self.ball_dxy_init
        else:
            self.ball_dx = self.ball_dxy_init * -1
            self.ball_dy = self.ball_dxy_init * -1

    def increaseSpeed(self):
        if(self.ball_dx >= 0):
            self.ball_dx = self.ball_dx + self.ball_dxy_speed_increment
            self.ball_dy = self.ball_dy + self.ball_dxy_speed_increment
        else:
            self.ball_dx = self.ball_dx - self.ball_dxy_speed_increment
            self.ball_dy = self.ball_dy - self.ball_dxy_speed_increment

    def checkBorderCollision(self):
        if self.ball.ycor() > 230:   # Right top paddle Border
            self.ball.sety(230)
            self.ball_dy = self.ball_dy * -1
            return
            
        if self.ball.ycor() < -230:  # Left top paddle Border
            self.ball.sety(-230)
            self.ball_dy = self.ball_dy * -1
            return 

        if self.ball.xcor() > 390:   # right width paddle Border
            self.ball.goto(0,0)
            self.ball_dx = self.ball_dx * -1
            self.resetSpeed()
            return Constants.PLAYER_NAME_DOKTORDUDE

        if(self.ball.xcor()) < -390: # Left width paddle Border
            self.ball.goto(0,0)
            self.ball_dx = self.ball_dx * -1
            self.resetSpeed()
            return Constants.PLAYER_NAME_DUDE

    def checkPaddleCollision(self):
        if(self.ball.xcor() > 340) and (self.ball.xcor() < 350) and (self.ball.ycor() < self.game.player_b.paddle.ycor() + 100 and self.ball.ycor() > self.game.player_b.paddle.ycor() - 100):
            self.ball.setx(340)
            self.ball_dx = self.ball_dx * -1
            self.increaseSpeed()

        if(self.ball.xcor() < -340) and (self.ball.xcor() > -350) and (self.ball.ycor() < self.game.player_a.paddle.ycor() + 100 and self.ball.ycor() > self.game.player_a.paddle.ycor() - 100):
            self.ball.setx(-340)
            self.ball_dx = self.ball_dx * -1
            self.increaseSpeed()

    def clear(self):
        self.ball.goto(0,0)
        self.ball_dx = self.ball_dxy_init
        self.ball_dy = self.ball_dxy_init