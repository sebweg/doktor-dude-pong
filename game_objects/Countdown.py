import turtle as t
import time

from Constants import Constants

# Countdown class
class Countdown():
    def __init__(self):
        self.is_running = True

        self.pen = t.Turtle()
        self.pen.speed(0)
        self.pen.color(Constants.COLOR_SCORE)
        self.pen.penup()
        self.pen.hideturtle()
        self.pen.goto(0,-40)
    
    def clear(self):
        self.pen.clear()
    
    def start(self):
        self.is_running = True

    def draw(self):
        if not self.is_running: return

        count = 3

        while count >= 0:

            self.pen.clear()

            if count > 0:
                self.pen.write("{}".format(count),align="center",font=('Monaco',100,"normal"))
            else:
                self.pen.write("GO!",align="center",font=('Monaco',100,"normal"))

            time.sleep(1)
            count = count - 1

        self.pen.clear()
        self.is_running = False


