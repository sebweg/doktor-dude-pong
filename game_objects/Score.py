import turtle as t

from Constants import Constants

# Score class
class Score:
    def __init__(self, player_a, player_b):
        self.player_a = player_a
        self.player_b = player_b

        self.pen = t.Turtle()
        self.pen.speed(0)
        self.pen.color(Constants.COLOR_SCORE)
        self.pen.penup()
        self.pen.hideturtle()
        self.pen.goto(0,200)
    
    def clear(self):
        self.pen.clear()

    def draw(self):
        self.pen.clear()
        self.pen.write(
            "{}: {}                    {}: {} "
            .format(self.player_a.name, self.player_a.score, self.player_b.name, self.player_b.score),
            align="center",font=('Monaco',24,"normal")
        )