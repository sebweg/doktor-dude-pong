# Constants Class
class Constants:
    TITLE = "Doktor Dude Pong"

    MENU_BUTTON_NEW_GAME = "New Game"
    MENU_BUTTON_QUIT_GAME = "Quit Game"

    IMG_DOKTORDUDE = "images/doktordude.gif"
    IMG_DUDE = "images/dude.gif"
    IMG_BOWLING_BALL = "images/bowlingball.gif"
    IMG_BOWLING_BG = "images/bowling.gif"

    PLAYER_NAME_DOKTORDUDE = "Doktor Dude"
    PLAYER_NAME_DUDE = "Dude"

    SOUND_WALLHIT = "sounds/wallhit.wav"
    SOUND_PADDLEHIT = "sounds/paddle.wav"
    SOUND_BACKGROUND = "sounds/bobdylan_themaninme.mp3"
    SOUND_SHUFFLE_ARRAY = [
        "sounds/chopsuey.wav",
        "sounds/dudewtf.wav",
        "sounds/letsgo.wav",
        "sounds/playingadude.wav",
        "sounds/haltstop.wav",
        "sounds/bazinga.wav",
        "sounds/needadoktor.wav",
        "sounds/lena.wav",
        "sounds/laura.wav",
        "sounds/noah.wav",
        "sounds/oskar.wav",
        "sounds/coco.wav",
        "sounds/alva.wav"
    ]

    COLOR_SCORE = "skyblue"
    COLOR_TITLE = "yellow"
    COLOR_BUTTON = "cyan"

    MOVIE_INTRO = "images/intro.mp4"
    MOVIE_INTRO_AUDIO = "sounds/intro.mp3"

    RASPBERRYPI_HOSTNAME = "raspidude"

    GPIO_PIN_PLAYER_A_UP = 13       # GPIO13 - no. 33
    GPIO_PIN_PLAYER_A_DOWN = 11     # GPIO11 - no. 23
    GPIO_PIN_PLAYER_A_SOUND = 26    # GPIO26 - no. 37

    GPIO_PIN_PLAYER_B_UP = 16       # GPIO16 - no. 36
    GPIO_PIN_PLAYER_B_DOWN = 20     # GPIO20 - no. 38
    GPIO_PIN_PLAYER_B_SOUND = 12    # GPIO21 - no. 32

    GPIO_PIN_MENU = 5               # GPIO5 - no. 29
    GPIO_PIN_SUBMIT= 6              # GPIO6 - no. 31

    GPIO_BOUNCETIME = 200