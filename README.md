# Doktor Dude Pong

It is multiplayer ping pong game build on turtle module focused on the dude and Doktor Dude Lukas Kater. Two persons can play this game.

Intro video in high quality can be obtained [here](https://drive.google.com/file/d/1AXSNp2p22uDsgxZI9TANh4VT1y5BDGvu/view).

### Game Start Window

![](images/Start_Point.png)

## Controls

### Arrow Keys for Direction:

#### For Player A:

	To move paddle up press 'e'
	To move paddle down press 'd'
	To play a sound press 'r'

### For Player B:

	To move paddle up press 'UP_ARROW'
	To move paddle down press 'DOWN_ARROW'
	To play a sound press 'p'

### For Navigation in Menu:

   	To move up press 'UP_ARROW'
	To move down press 'DOWN_ARROW'
	To quit press 'q'
	To submit press 'SPACE'

## Dependencies:

1. Python v3.x is required.
2. turtle module is also required.
3. Raspberry Pi: RPi.GPIO and pygame is required.
4. To play Videos: OpenCV2 and Pillow 
(https://medium.com/@yunusmuhammad007/2-raspberry-pi-install-opencv-pada-python-3-7-menggunakan-pip3-a2504dffd984)

## Raspberry Pi Constructions:

### Desktop shortcut

1. Desktop shortcut for Doktor Dude Pong is the file DoktorDudePong.desktop.
2. Copy the file to raspberry pi desktop
3. Make the python file executable

	chmod +x /home/pi/doktor-dude-pong/Main.py
	
4. Start the game with the desktop shortcut

### GPIO pins

	Player A up = GPIO13 - no. 33
    Player A down = GPIO11 - no. 23
    Player A sound = GPIO26 - no. 37

    Player B up = GPIO16 - no. 36
    Player B down = GPIO20 - no. 38 
    Player B sound = GPIO12 - no. 32

    quit = GPIO5 - no. 29
    submit = GPIO6 - no. 31

	Speaker:
	GPIO21 - no. 40
	GPIO18 - no. 12
	GPIO19 - no. 35

	To test the GPIOs run the 'python3 RPIOTest.py' command
	The pins and bouncing time can be modified in 'Constants.py'

## Disclaimer:

This project is Open Source licensed. The dude likes that.
