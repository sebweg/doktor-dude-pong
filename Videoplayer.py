import tkinter
import cv2
from PIL import Image, ImageTk

from Soundplayer import Soundplayer, playSound

def playVideo(movie_file, music_file):
    cap = cv2.VideoCapture(movie_file)

    root = tkinter.Tk()
    root.geometry("800x480")
    root.overrideredirect(True)
    lmain = tkinter.Label(root)
    lmain.pack()

    def show_frame():
        ret, frame = cap.read()

        if not ret:
            soundplayer.stop()
            root.destroy()
            return

        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        #cv2image = cv2.resize(cv2image, (800, 480))
        img = Image.fromarray(cv2image)
        imgtk = ImageTk.PhotoImage(image=img)
        lmain.imgtk = imgtk
        lmain.configure(image=imgtk)
        lmain.after(10, show_frame)

    soundplayer = Soundplayer(music_file, False)
    soundplayer.start()

    show_frame()
    root.mainloop()